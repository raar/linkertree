

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss

|FluxWeb| `RSS <https://raar.frama.io/linkertree/rss.xml>`_

.. _linkertree:
.. _links:
.. _raar_liens:

======================================================================================
Liens RAAR **Réseau d'Actions contre l'Antisémitisme et tous les Racismes** |raar|
======================================================================================

- https://raar.info/ **(blog officiel)** , https://raar.info/feed/ (flux Web)
- https://twitter.com/RAAR2021 (https://nitter.poast.org/RAAR2021/rss)
- https://www.facebook.com/RAAR2021 (officiel)
- https://www.instagram.com/raar_france/ (officiel)
- https://kolektiva.social/@raar (officiel), https://kolektiva.social/@raar.rss (Flux Web)
- https://www.youtube.com/@raar2800/videos (Vidéos RAAR sur Youtube)
- https://www.youtube.com/@GlobSubLife/videos (Vidéos RAAR sur Youtube)
- https://www.youtube.com/@GlobSubYouTube/videos (Vidéos RAAR sur Youtube)
- https://blogs.mediapart.fr/raar
- https://blogs.mediapart.fr/raar/blog
- https://blogs.mediapart.fr/albert-herszkowicz
- https://www.helloasso.com/associations/reseau-d-actions-contre-l-antisemitisme-et-tous-les-racismes (pour adhérer en ligne)
- raar@riseup.net (courriel du RAAR)
- :ref:`antisem:antisem`

**Blog officiel raar.info**
==============================

- https://raar.info/



Manifeste du RAAR
========================

- :ref:`raar_info:raar_manifeste`


Nous rejoindre (helloasso)
-----------------------------------

- https://raar.info/nous-rejoindre/
- https://www.helloasso.com/associations/reseau-d-actions-contre-l-antisemitisme-et-tous-les-racismes (pour adhérer en ligne)

.. figure:: images/helloasso.webp
   :width: 700

   https://www.helloasso.com/associations/reseau-d-actions-contre-l-antisemitisme-et-tous-les-racismes

- Collectif de luttes contre l'antisémitisme et tous les racismes.
- Réflexion sur l'histoire et l'actualité de l'antisémitisme.
- Organisation de webinaires.
- Rédaction de communiqués et de tribunes dans la presse.
- Publications diverses.
- Organisation d'initiatives publiques.
- Formation.

Vous partagez nos convictions et nos valeurs.
Vous souhaitez agir et rejoindre le RAAR
Réseau d’Actions contre l’Antisémitisme et tous les Racismes

Vous avez la possibilité d’adhérer en ligne via Helloasso suivant `ce lien https://www.helloasso.com/associations/reseau-d-actions-contre-l-antisemitisme-et-tous-les-racismes <https://www.helloasso.com/associations/reseau-d-actions-contre-l-antisemitisme-et-tous-les-racismes>`_

ou vous pouvez télécharger le bulletin d’adhésion en cliquant `ici https://raar.info/wp-content/uploads/2022/03/RAAR-BULLETIN-DADHESION.pdf <https://raar.info/wp-content/uploads/2022/03/RAAR-BULLETIN-DADHESION.pdf>`_  puis l’envoyer à l’adresse postale :

::

    RAAR
    7 rue Bernard de Clairvaux
    75003 Paris



Agendas
==========

.. toctree::
   :maxdepth: 3

   agendas/agendas

raar.frama.io infos **(archives non officielles et indépendantes du RAAR)**
==============================================================================

- https://raar.frama.io/linkertree
- https://raar.frama.io/raar-info-2024
- https://raar.frama.io/raar-info-2023
- https://raar.frama.io/raar-info (2021-2022)
- https://luttes.frama.io/contre/l-antisemitisme/

.. _communiques_raar:

|RaarCommunique| Communiqués et déclarations du RAAR
--------------------------------------------------------

.. _communiques_raar_2024:

|RaarCommunique| Communiqués/déclarations du RAAR en 2024
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- :ref:`raar_2024:raar_com_2024_06_21`
- :ref:`raar_2024:raar_com_2024_06_14`
- :ref:`raar_2024:raar_com_2024_06_10`
- :ref:`raar_2024:raar_com_2024_06_03`
- :ref:`raar_2024:declaration_raar_2024_05_30`
- :ref:`raar_2024:raar_com_2024_05_28`
- :ref:`raar_2024:raar_com_2024_05_27`
- :ref:`raar_2024:raar_com_2024_05_23`
- :ref:`raar_2024:raar_2024_05_17`
- :ref:`raar_2024:raar_2024_05_15`
- :ref:`raar_2024:raar_2024_05_04`
- :ref:`raar_2024:raar_2024_05_02`
- :ref:`raar_2024:raar_2024_04_19`
- :ref:`raar_2024:raar_2024_04_15`
- :ref:`raar_2024:raar_2024_03_23`
- :ref:`raar_2024:raar_2024_03_21`
- :ref:`raar_2024:raar_2024_03_14`
- :ref:`raar_2024:raar_2024_03_12`
- :ref:`raar_2024:raar_2024_03_09`
- :ref:`raar_2024:raar_2024_03_02`
- :ref:`raar_2024:raar_2024_02_15`

.. _communiques_raar_2023:

Communiqués du RAAR en 2023
+++++++++++++++++++++++++++++

- :ref:`raar_2023:communique_2023_12_31`
- :ref:`raar_2023:communique_2023_12_20`
- :ref:`raar_2023:communique_2023_12_08`
- :ref:`raar_2023:communique_2023_12_05`
- :ref:`raar_2023:communique_2023_11_24`

Communiqués du RAAR 2021
+++++++++++++++++++++++++++++

- :ref:`raar_info:raar_communique_2021_01_21`


.. _rencontres_raar:

Rencontres publiques, commémorations et webinaires du RAAR
----------------------------------------------------------------

- :ref:`raar_2024:golem_jjr_raar_2024_05_23` ❤️
- :ref:`raar_2024:raar_2024_03_17`
- :ref:`raar_2024:raar_2024_01_28`
- :ref:`raar_2023:webinaire_2023_12_18`
- :ref:`raar_2023:nuit_de_cristal_2023_11_09`
- :ref:`raar_2023:raar_2023_10_15`

Videos du RAAR sur Youtube
=============================

- https://www.youtube.com/@raar2800/videos (Vidéos RAAR sur Youtube)

Contre l'antisémitisme
============================

- https://luttes.frama.io/contre/l-antisemitisme/

**Leftrenewal** (Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche)
======================================================================================================================================

- https://leftrenewal.frama.io/linkertree/
- https://leftrenewal.frama.io/leftrenewal-info/


Dessins de Joann Sfar
========================

- http://joann-sfar.frama.io/joann-sfar-2023


Collectif Golem
==================

- :ref:`golem:golem`



Organisations juives luttant contre l'antisémitisme
=======================================================

- https://luttes.frama.io/contre/l-antisemitisme/organisations/organisations.html


Centre Bernard Lazare Grenoble (CBL Grenoble)
-----------------------------------------------------

- https://cbl.frama.io/cbl-grenoble/linkertree/


Centre Bernard Lazare Paris  (CBL Paris)
-------------------------------------------

- https://www.cerclebernardlazare.org/


Collectif Golem
---------------------

- :ref:`golem:golem`

JJR
----

- http://jjr.frama.io/juivesetjuifsrevolutionnaires


La Paix Maintenant
--------------------

- https://www.lapaixmaintenant.org/liens/

Memorial98
--------------

- http://www.memorial98.org/



ORAAJ
--------

- https://oraaj.frama.io/oraaj-info/



K la revue
------------------

- https://k-larevue.com/

Ni patrie, ni frontières (NPNF) de Yves Coleman
-----------------------------------------------------------

- :ref:`antisem:yves_coleman`
- :ref:`raar_2024:coleman_textes_2024_05_29`

De nombreux autres articles (des traductions car la discussion est beaucoup
plus avancée en dehors des frontières mentales étriquées de l’Hexagone) se
trouvent dans les rubriques du site consacrées:

- `à l’antisémitisme <https://npnf.eu/spip.php?rubrique13>`_,
- `à l’antisionisme <https://npnf.eu/spip.php?rubrique29>`_
- et à `Israël/Palestine <https://npnf.eu/spip.php?rubrique45>`_

Yves Coleman, Ni patrie ni frontières, 29 mai 2024


Les podcasts de Martin Eden
---------------------------------

- :ref:`antisem:martin_eden`



Solidarite judeo-arabe (Belgique)
-----------------------------------------

- :ref:`antisem:sjo`


Nous vivrons #🟦
--------------------

- https://nous-vivrons.fr/

Luttes pour la Paix (Guerrières de la paix, Standing together Women Wage Peace (WWP))
==========================================================================================

- https://luttes.frama.io/pour/la-paix/linkertree

**Guerrières de la paix**
----------------------------

- https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/


.. _stanging_together:

**Standing together** (omdim beyachad)
--------------------------------------------


- https://www.standing-together.org/about-us
- https://www.instagram.com/standing.together.movement/
- https://www.instagram.com/standing.together.english/
- https://www.youtube.com/@standingtogether7105/videos
- https://leftodon.social/@omdimbeyachad

Women Wage Peace (WWP)
------------------------

- https://luttes.frama.io/pour/la-paix/womenwagepeace/

Organisations luttant contre l'antisémitisme
===============================================

LDH
----

- https://www.ldh-france.org/sujet/racisme-antisemitisme/
- http://luttes.frama.io/pour/les-droits-humains/linkertree


CNCDH
------

- https://luttes.frama.io/pour/les-droits-humains/cncdh/

Alarmer
-------------

- https://alarmer.org/

Musée de la Résistance de l'Isère
---------------------------------------

- http://grenoble.frama.io/musee-de-la-resistance-et-de-la-deportation-de-l-isere

.. _orga_anti_racistes:

**Organisations luttant contre le racisme**
=============================================

antiracisme-solidarite
-------------------------

- https://antiracisme-solidarite.org/
- https://antiracisme-solidarite.org/agenda/
- https://antiracisme-solidarite.org/nos-visuels/
- https://www.instagram.com/campagne_solidarite/
- https://www.youtube.com/@AntiracismeEtSolidarite/videos
- https://nitter.cz/MSolidarites/rss
- https://blogs.mediapart.fr/marche-des-solidarites


La Cimade
-----------

- https://www.lacimade.org/
- https://nitter.cz/lacimade/rss


.. _gisti:

GISTI (Groupe d’Information et de Soutien des Immigrés)
----------------------------------------------------------------

- https://blogs.mediapart.fr/association-gisti
- https://piaille.fr/@gisti (https://piaille.fr/@gisti.rss, flux web)
- https://www.gisti.org/spip.php?page=sommaire
- http://www.gisti.org/spip.php?page=backend&id_rubrique=0 (flux Web RSS)
- https://www.gisti.org/spip.php?rubrique38 (plein droit)

Musée de l'histoire de l'immigration
--------------------------------------

- https://www.histoire-immigration.fr/
- https://www.histoire-immigration.fr/l-echo-des-archives


Dossier Sionisme/antisionisme
=================================

- https://israel.frama.io/sionisme-antisionisme/


Conflit Israël/Palestine
==================================

- https://conflits.frama.io/israel-palestine/linkertree/

