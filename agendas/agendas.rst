
.. index::
   pair: Agenda ; Mémorial de la Shoah


.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://qoto.org/@noamsw"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>

.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss


.. _agendas:

================================================================================
**Agendas**
================================================================================


Mémorial de la Shoah
======================

- https://billetterie.memorialdelashoah.org/fr/evenements-et-ateliers
- https://www.memorialdelashoah.org/programme-bimestriel/2024-janvier-mars/

Antiracisme Solidarite
============================

- https://antiracisme-solidarite.org/agenda/

La Cimade
===========

- https://www.lacimade.org/agenda/


Tous migrants
================

- https://tousmigrants.weebly.com/agenda.html

Framagenda raar-info
==========================

- https://framagenda.org/u/raar-info

